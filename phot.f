       program phot
c
       integer pgbeg, pgcurs, k, n, m, args
       parameter ( k = 4, n = 821, m = 1024, lc = 1084 )
c filter constants
       real vx, bx, rx, ix
       real vwidlo, vwidhi, bwidlo, bwidhi
       real rwidlo, rwidhi, iwidhi, iwidlo
c model and plot parameters
       logical instruct, ok, help, onlyModel
       real T, starMax, intgMx
       integer camera, mirror, oldMir
       character label*30, surface*13, source*32, camtxt*3, filenm*21
       character*1 go, comment, xserve, scaler, lines, metal
c external file vectors
       real counts(k), errors(k), times(k)
       real bessr(n), bessv(n), bessb(n), bessi(n)
       real bessrl(n), bessvl(n), bessbl(n), bessil(n)
       real bess(k,2,n)
       real spcwl(lc), spcamp(lc)
c internal plotting
       real nm(m), qem(m), btm(m), metalm(m), nphotm(m)
       real tot(4,n), vtot(n), btot(n), rtot(n), itot(n)
       real integ(k), mxval
       integer mxloc
c functions
       real al45, al00, ni45, ni00, refl, ag45, ag00
       real qe, nphot, bt, ap6, ap8, src
       character*21 filer
c
c file i/o read in data
c
       open ( unit = 10, file="phot.dat" )
       read (10,'(A32)') source
       read (10,*) T
       read (10,'(A1)') lines
       read (10,'(A1)') metal
       read (10,'(I1)') camera
       read (10,'(I1)') mirror
       if ( metal .eq. 'T' ) then
               mirror = 0
       else if ( mirror .eq. 0 ) then
               metal = 'T'
       end if
c
       i = 1
10     if ( i .le. k ) then
110     format ( A1, E10.3, E10.3, F5.1 )
        read (10,110) comment, counts(i), errors(i), times(i)
        if ( comment .eq. 'D' ) then
             if ( times(i) .ne. 0.0 ) then
              counts(i) = counts(i)/times(i)
              errors(i) = errors(i)/times(i)
             end if
           i = i + 1
           goto 10
        else
           goto 10
        end if
       end if
       close (10)
c
c bessel curves from
c http://monet.uni-goettingen.de/foswiki/MonetObserving/Filters
c do not agree with these files provided by Custom Scientific
c
       open ( unit = 11, file="materials/bess-r.custom" )
       open ( unit = 12, file="materials/bess-v.custom" )
       open ( unit = 13, file="materials/bess-b.custom" )
       open ( unit = 14, file="materials/bess-i.custom" )
       do i = 1, n
        read ( 11,*) bess(3,1,i), bess(3,2,i)
        read ( 12,*) bess(1,1,i), bess(1,2,i)
        read ( 13,*) bess(2,1,i), bess(2,2,i)
        read ( 14,*) bess(4,1,i), bess(4,2,i)
        do j = 1, k
         bess(j,2,i) = 1.0E-02*bess(j,2,i)
        end do
       end do
       close (11)
       close (12)
       close (13)
       close (14)
c
c filter constants, peak of trasmission and half of peak
       bx = 427.0
       vx = 516.0
       rx = 597.0
       ix = 825.0
       bwidlo = 44.0
       bwidhi = 55.0
       vwidlo = 26.0
       vwidhi = 61.0
       rwidlo = 27.0
       rwidhi = 96.0 
       iwidlo = 95.0
       iwidhi = 208.0
c
c parse command line request for direct-to-postscript, skipping xwindow
       args = iargc()
       call getarg(1, xserve)
       if ( args .gt. 0 .and. xserve .eq. 'p' ) then
               go = 'p'
       else
               go = 'z'
       end if
c
c
c ---=== beginning of interactive loop ===---
c
c integrals and total product for plotting
       instruct = .true.
       help = .true.
       scaler = 'U'
       oldMir = 2
200    filenm = filer(T,lines,label)
       if ( lines .eq. 'l' .or. lines .eq. 'L' ) then
         open ( unit = 20, file = filenm )
         do i = 1, lc
          read (20,*) spcwl(i), spcamp(i)
         end do
         call valloc(lc,spcamp,mxval,mxloc)
         do i = 1, lc
          spcamp(i) = spcamp(i)/mxval
         end do
         close(20)
       end if
       do j = 1, k
        integ(j) = 0.0
        tot(j,1) = 0.0
        do i = 2, n
         integ(j) = integ(j) +
     $   ( bess(j,2,i-1)*qe(bess(j,1,i-1),camera)*
     $     src(bess(j,1,i-1),T,lines,spcwl,spcamp)*
     $     nphot(bess(j,1,i-1))*refl(bess(j,1,i-1),metal,mirror) +
     $     bess(j,2,i)*qe(bess(j,1,i),camera)*
     $     src(bess(j,1,i),T,lines,spcwl,spcamp)*
     $     nphot(bess(j,1,i))*refl(bess(j,1,i),metal,mirror)
     $   ) / 2.0 * ( bess(j,1,i) - bess(j,1,i-1) )
         tot(j,i)=bess(j,2,i)*qe(bess(j,1,i),camera)*
     $     src(bess(j,1,i),T,lines,spcwl,spcamp)*
     $     nphot(bess(j,1,i))*refl(bess(j,1,i),metal,mirror)
        end do
       end do
c
c autoscaling
c
       onlyModel = .false.
       call valloc(k,counts,mxval,mxloc)
       if ( scaler .eq. 'B' .and. counts(2) .ne. 0.0 ) then
               mxloc = 2
       else if ( scaler .eq. 'V' .and. counts(1) .ne. 0.0 ) then
               mxloc = 1
       else if ( scaler .eq. 'R' .and. counts(3) .ne. 0.0 ) then
               mxloc = 3
       else if ( scaler .eq. 'I' .and. counts(4) .ne. 0.0 ) then
               mxloc = 4
       else if ( mxval .eq. 0.0 ) then
               onlyModel = .true.
       end if
c
       if ( onlyModel ) then
               call valloc(k,integ,mxval,mxloc)
               starMax = mxval
       else
               intgMx = integ(mxloc)
               do i = 1, k
                integ(i) = integ(i)*counts(mxloc)/intgMx
               end do
               starMax = mxval
               call valloc(k,integ,mxval,mxloc)
               if ( mxval .gt. starMax ) then
                       starMax = mxval
               end if
       end if
c
c model vectors for plotting
       do i = 1, m
        nm(i) = 290.0 + real(i)/real(m)*1020.0
        btm(i) = src(nm(i), T, lines, spcwl, spcamp)
        qem(i) = qe(nm(i),camera)
        metalm(i) = refl(nm(i),metal,mirror)
        nphotm(i) = nphot(nm(i))
       end do
       do i = 1, n
        bessrl(i) = bess(3,1,i)
        bessr(i) = bess(3,2,i)
        rtot(i) = tot(3,i)
        bessvl(i) = bess(1,1,i)
        bessv(i) = bess(1,2,i)
        vtot(i) = tot(1,i)
        bessbl(i) = bess(2,1,i)
        bessb(i) = bess(2,2,i)
        btot(i) = tot(2,i)
        bessil(i) = bess(4,1,i)
        bessi(i) = bess(4,2,i)
        itot(i) = tot(4,i)
       end do
c
c temperature label string
       write (camtxt, '(A,I1)') 'AP', camera
       if ( metal .eq. 'A' ) then
               write(surface,'(A10,I1,A2)')'A\(0677)\u',mirror,'\d'
       else if ( metal .eq. 'N' ) then
               write(surface,'(A10,I1,A2)')'N\(2159)\u',mirror,'\d'
       else if ( metal .eq. 'S' ) then
               write(surface,'(A10,I1,A2)')'A\(0657)\u',mirror,'\d'
       else if ( metal .eq. 'T' ) then
               write(surface,*)'CaF\d2\u'
       end if
c
c
c
c  ---=== plotting ===---
c
c
c begin pgplot
       if ( go .eq. 'p' .or. go .eq. 'P' ) then
c               if ( pgbeg(0,'star.gif/GIF',1,1) .ne. 1 ) stop
               if ( pgbeg(0,'star.ps/CPS',1,1) .ne. 1 ) stop
       else
        if ( pgbeg(0,'/XWINDOW',1,1) .ne. 1 ) stop
        if ( instruct ) then
        write(*,*) ''
        write(*,*) 'Type "q" in the window to quit.'
        write(*,*) 'Type "6" or "8" to choose between AP6 and AP8 '
     $          // 'cameras.'
        write(*,*) 'Type "2" or "3" to choose number of mirror '
     $          // 'surfaces or "0" for a refractor.'
        write(*,*) 'Type "a" or "n" or "s" for mirror coatings: '
     $          // ' Al, Ni, or Ag.'
        write(*,*) 'Type "v", "b", "r", or "i" to scale model to '
     $          // 'that data point.'
        write(*,*) 'Type "u" for default autoscaling.'
        write(*,*) 'Type "t" to enter temperature manually.'
        write(*,*) 'Type "k" for a blackbody source or "l" for a '
     $          // 'best match library spectrum.'
        write(*,*) 'Type "p" to write plot to '
     $          // 'star.ps and parameters to phot.out, then quit.'
        write(*,*) 'Type "h" for these instructions.'
        write(*,*) ''
        help = .false.
        end if
       end if
c
       call pgask ( .false. )
c
       call pgsvp(  0.15, 0.95, 0.45, 0.95 )
       call pgslw(3)
       call pgsch(1.5)
       call pgswin( 345.0, 1055.0, 0.0, starMax+0.1*starMax )
       call pgbox ( 'bcts', 100.0, 2, 'bcnts', 0.0, 0 )
       call pglab ('',
     $              'data (\m2) and model (\m3)',
     $              '')
c
       call pgsci(1)
       call pgptxt(1005.0, 0.1*starMax, 0.0, 1.0, source)
c
       call pgsci(4)
       call pgerr1(6,bx,counts(2),errors(2),0.9)
       call pgerr1(3,bx,counts(2),bwidlo,0.0)
       call pgerr1(1,bx,counts(2),bwidhi,0.0)
       call pgpt1(bx,integ(2),3)
c
       call pgsci(3)
       call pgerr1(6,vx,counts(1),errors(1),0.9)
       call pgerr1(3,vx,counts(1),vwidlo,0.0)
       call pgerr1(1,vx,counts(1),vwidhi,0.0)
       call pgpt1(vx,integ(1),3)
c
       call pgsci(2)
       call pgerr1(6,rx,counts(3),errors(3),0.9)
       call pgerr1(3,rx,counts(3),rwidlo,0.0)
       call pgerr1(1,rx,counts(3),rwidhi,0.0)
       call pgpt1(rx,integ(3),3)
c
       call pgscr(13, 0.545, 0.270, 0.074)
       call pgsci(13)
       call pgerr1(6,ix,counts(4),errors(4),0.9)
       call pgerr1(3,ix,counts(4),iwidlo,0.0)
       call pgerr1(1,ix,counts(4),iwidhi,0.0)
       call pgpt1(ix,integ(4),3)
c
       call pgsci(1)
c
c second panel
c
       call pgsvp(  0.15, 0.95, 0.15, 0.45 )
       call pgslw(3)
       call pgsch(1.5)
       call pgswin( 345.0, 1055.0, 0.0, 1.2 )
       call pgbox ( 'bcnts', 100.0, 2, 'bcmvts', 1.0, 5 )
       call pglab ('wavelength (nm)',
     $              'models',
     $              '')
c
       if ( go .eq. 'p' .or. go .eq. 'P' ) then
               call pgsfs(3)
       else
               call pgsfs(1)
       end if
       call pgsci(13)
       call pgpoly(n,bessil,itot)
       call pgsci(2)
       call pgpoly(n,bessrl,rtot)
       call pgsci(3)
       call pgpoly(n,bessvl,vtot)
       call pgsci(4)
       call pgpoly(n,bessbl,btot)
c
       call pgscr(15, 0.41, 0.41, 0.41)
       call pgsci(15)
       call pgtext(305.0, 0.20, 'QE')
       call pgtext(690.0, 1.01, camtxt)
       call pgline(m,nm,qem)
       call pgsci(8)
       call pgtext(305.0, 0.80, '\fiB\fn\d\fiT\fn\u')
       call pgtext(365.0, 1.01, label)
       call pgline(m,nm,btm)
c       call pgsci(12)
c       call pgline(m,nm,nphotm)
       call pgsci(6)
       if ( mirror .eq. 0 .or. metal .eq. 'T' ) then
        call pgtext(305.0, 0.50, '\frt\fn\u2\d')
       else
        call pgtext(305.0, 0.50, '\frr\fn\u2\d')
       end if
       call pgtext(975.0, 1.01, surface)
       call pgline(m,nm,metalm)
c
       call pgsci(2)
       call pgline(n,bessrl,bessr)
       call pgsci(3)
       call pgline(n,bessvl,bessv)
       call pgsci(4)
       call pgline(n,bessbl,bessb)
       call pgsci(13)
       call pgline(n,bessil,bessi)
c
c program runs in xwindow by default
c typing any character besides 'q' or 'p' replots in window
c typing [lowercase] q quits out of the program
c typing p writes the view to the star.ps file
       ok = .false.
300    continue
       if ( go .eq. 'p' .or. go .eq. 'P' ) then
          open ( unit = 15, file="phot.out" )
          write (15,'(A32)') source
          write (15, '(F5.0)') T
          write (15,'(A1)') metal
          write (15,'(I1)') camera
          write (15,'(I1)') mirror
310       format ( A1, E10.3, E10.3, F5.1 )
          comment = 'D'
          do i = 1, k
           write (15,310) comment, counts(i), errors(i), times(i)
          end do
          close (15)
c
          call pgend
       else if ( pgcurs(xx,yy,go) .eq. 1 .and. go .ne. 'q' ) then
               if ( go .eq. 'p' .or. go .eq. 'P' ) then
                       ok = .true.
               else if ( go .eq. 'a' .or. go .eq. 'A' ) then
                       metal = 'A'
                       if ( mirror .eq. 0 ) then
                         mirror = oldMir
                       end if
                       ok = .true.
               else if ( go .eq. 'n' .or. go .eq. 'N' ) then
                       metal = 'N'
                       if ( mirror .eq. 0 ) then
                         mirror = oldMir
                       end if
                       ok = .true.
               else if ( go .eq. 's' .or. go .eq. 'S' ) then
                       metal = 'S'
                       if ( mirror .eq. 0 ) then
                         mirror = oldMir
                       end if
                       ok = .true.
               else if ( go .eq. '0' ) then
                       oldMir = mirror
                       mirror = 0
                       metal = 'T'
                       ok = .true.
               else if ( go .eq. '2' .and. mirror .ne. 0 ) then
                       mirror = 2
                       ok = .true.
               else if ( go .eq. '3' .and. mirror .ne. 0 ) then
                       mirror = 3
                       ok = .true.
               else if ( go .eq. '6' ) then
                       camera = 6
                       ok = .true.
               else if ( go .eq. '8' ) then
                       camera = 8
                       ok = .true.
               else if ( go .eq. 'u' .or. go .eq. 'U' ) then
                       scaler = 'U'
                       ok = .true.
               else if ( go .eq. 'v' .or. go .eq. 'V' ) then
                       scaler = 'V'
                       ok = .true.
               else if ( go .eq. 'b' .or. go .eq. 'B' ) then
                       scaler = 'B'
                       ok = .true.
               else if ( go .eq. 'r' .or. go .eq. 'R' ) then
                       scaler = 'R'
                       ok = .true.
               else if ( go .eq. 'i' .or. go .eq. 'I' ) then
                       scaler = 'I'
                       ok = .true.
               else if ( go .eq. 't' .or. go .eq. 'T' ) then
                       write(*,*) 'Enter temperature in Kelvin: '
                       read(*,*) T
                       write(*,*) ''
                       ok = .true.
               else if ( go .eq. 'l' .or. go .eq. 'L' ) then
                       lines = 'L'
                       ok = .true.
               else if ( go .eq. 'k' .or. go .eq. 'K' ) then
                       lines = 'K'
                       ok = .true.
               else if ( go .eq. 'h' .or. go .eq. 'H' ) then
                       help = .true.
                       ok = .true.
               end if
               if ( ok ) then
                if (help) then
                 instruct = .true.
                else
                 instruct = .false.
                end if
                goto 200
               else
                goto 300
               end if
       else
               call pgend
       end if
c
c
       stop
       end
c end of program
c
c
c
c --== functions ==--
c
c gnuplot fits to these functions - formal errors in fit logs
c
c for x in nanometers, quantum efficiency between 0 and 1 of ap8
c fitted data from http://www.lulin.ncu.edu.tw/lot/ApogeeCCD.htm
c
       real function ap8(x)
       real x
       real fa, fb, fc, fd, fe, ff, fg, fh, fi, fj, fk, fl
c
       fa = 1.18749   
       fb = 0.00284406
       fc = 0.616084  
       fd = 0.994442  
       fe = 1.1538    
       ff = 1.04518   
       fg = 0.86965   
       fh = 0.564171  
       fi = 0.294293  
       fj = 0.099093  
       fk = 0.0146714 
       fl = -0.0097588
c
       ap8 = fa*sin(fb*x) + fc*sin(2.*fb*x) + fd*sin(3.*fb*x) +
     $  fe*sin(4.*fb*x) + ff*sin(5.*fb*x) + fg*sin(6.*fb*x) +
     $  fh*sin(7.*fb*x) + fi*sin(8.*fb*x) + fj*sin(9.*fb*x) +
     $  fk*sin(10.*fb*x) + fl*sin(11.*fb*x)
c
       return
       end
c
c blackbody curve for input wavelngth in nm
       real function bt(x,T)
       real x, T
       real h, c, k, pi, b, xmax, btmax
c
       b = 2.8977685E-03
       h = 6.626E-34
       c = 2.998E+08
       k = 1.38E-23
       pi = 3.14159
       xmax = b/T
c
       if ( xmax .lt. 345.0E-09 ) then
               xmax = 345.0E-09
       end if
c
       x = x*1E-09
       bt = 8.0*pi*h*c/x**5/(exp(h*c/x/k/T)-1.0)
       btmax = 8.0*pi*h*c/xmax**5/(exp(h*c/xmax/k/T)-1.0)
       bt = bt/btmax
       x = x*1E+09
c       
       return
       end
c
c number of photons
       real function nphot(x)
       real x
       real h, c
c
       h = 6.626E-34
       c = 2.998E+08
c
c       nphot = x/h/c*1E-23
       nphot = (x/h/c)/(1055.0/h/c)
c
       return
       end
c
c aluminum mirror reflectance at 45 degrees bewteen 0 and 1 for input in nm
c fitted data from http://www.luxpop.com/
       real function al45(x)
       real x
       real a, b, c, d, e, f, g, a2, b2, c2, d2, e2
c
       a = -0.0632283
       b = 5162.34
       c = 833.873
       d = 1.19811
       e = -0.00108407
       f = 1.27669e-06
       g = -4.44819e-10
c
       a2 = 1.00987
       b2 = -0.00077566
       c2 = 2.4541e-06
       d2 = -3.24911e-09
       e2 = 1.44183e-12
c
       if ( x .ge. 700.0 ) then
        al45 = a*exp(-1.*(x-c)*(x-c)/2./b) + d + e*x + f*x**2 + g*x**3
       else 
        al45 = a2 + b2*x + c2*x**2 + d2*x**3 + e2*x**4
       end if
c
       return
       end
c
c aluminum mirror reflectance at normal incidence for input in nm
c fitted data from http://www.luxpop.com/
       real function al00(x)
       real x
       real a, b, c, d, e, f, g, a1, b2, c2, d2, e2
c
       a2 = 0.988083
       b2 = -0.000568591
       c2 = 1.80632e-06
       d2 = -2.34898e-09
       e2 = 9.81824e-13
c
       a = -0.0608618
       b = 5147.8
       c = 833.864
       d = 1.18732
       e = -0.00102973
       f = 1.21454e-06
       g = -4.23496e-10
c
       if ( x .ge. 637.0 ) then
        al00 = a*exp(-1.*(x-c)*(x-c)/2./b) + d + e*x + f*x**2 + g*x**3
       else 
        al00 = a2 + b2*x + c2*x**2 + d2*x**3 + e2*x**4
       end if
c
       return
       end
c
c nickel mirror reflectance at 45 degrees for input in nm
c fitted data from http://www.luxpop.com/
       real function ni45(x)
       real x
       real a, b, c, d, e, f, g
c
       a = 9.45133e-18
       b = -4.8832e-14
       c = 1.01394e-10
       d = -1.07e-07
       e = 5.91449e-05
       f = -0.0152573
       g = 1.8618
c
       ni45 = a*x**6 + b*x**5 + c*x**4 + d*x**3 + e*x**2 + f*x + g
c
       return
       end
c nickel mirror reflectance at 0 degrees for input in nm
c fitted data from http://www.luxpop.com/
       real function ni00(x)
       real x
       real a, b, c, d, e, f, g
c
       a = 1.02906e-17
       b = -5.30132e-14
       c = 1.09742e-10
       d = -1.15466e-07
       e = 6.36693e-05
       f = -0.0164254
       g = 1.97386
c
       ni00 = a*x**6 + b*x**5 + c*x**4 + d*x**3 + e*x**2 + f*x + g
c
       return
       end
c
c silver mirror reflectance at 0 degrees for input in nm
c fitted data from http://www.luxpop.com/
       real function ag00(x)
       real x
       real a, b, c, d, e, f, g
c
       a = -9.98339e-18
       b = 5.4932e-14
       c = -1.2384e-10
       d = 1.46428e-07
       e = -9.58762e-05
       f = 0.033054
       g = -3.73385
c
       ag00 = a*x**6 + b*x**5 + c*x**4 + d*x**3 + e*x**2 + f*x + g
c
       return
       end
c
c silver mirror reflectance at 45 degrees for input in nm
c fitted data from http://www.luxpop.com/
       real function ag45(x)
       real x
       real a, b, c, d, e, f, g
c
       a = -1.04519e-17
       b = 5.70328e-14
       c = -1.2754e-10
       d = 1.49605e-07
       e = -9.71947e-05
       f = 0.0332626
       g = -3.73383
c
       ag45 = a*x**6 + b*x**5 + c*x**4 + d*x**3 + e*x**2 + f*x + g
c
       return
       end
c
c spline through ap6 chip qe from file
       real function ap6(x)
       real x
       real x0, x1, y0, y1, z0, z1
       integer nn, ii
       parameter ( nn = 78 )
       real ap6l(nn), ap6qe(nn)
c
       open ( unit = 20, file='materials/ap6.txt' )
       do ii = 1, nn
        read (20,*) ap6l(ii), ap6qe(ii)
       end do
       close(20)
c
       do ii = 3, nn
        if ( x.ge.ap6l(ii-1) .and. x.lt.ap6l(ii) ) then
                y0 = ap6qe(ii-1)
                y1 = ap6qe(ii)
                x0 = ap6l(ii-1)
                x1 = ap6l(ii)
                z0 = (y0-ap6qe(ii-2))/(x0-ap6l(ii-2))
                z1 = -1.*z0 + 2.*(y1-y0)/(x1-x0)
                ap6 = y0 + z0*(x-x0) + (z1-z0)/2./(x1-x0)*(x-x0)*(x-x0)
                return
        else if ( x.lt.ap6l(2) .or. x.ge.ap6l(nn) ) then
                ap6 = 0.0
                return
        end if
       end do
c
       return
       end
c
c qe selector
       real function qe(x, mm)
       real x
       integer mm
       real ap8, ap6
c
       if ( x .ge. 1100.0 .or. x .le. 300.0 ) then
               qe = 0.0
       else
        if ( mm .eq. 8 ) then
                qe = ap8(x)
        else if ( mm .eq. 6 ) then
                qe = ap6(x)
        end if
       end if
c
       return
       end
c
c metal selector
       real function refl(x,metal,mirror)
       real x
       character*1 metal
       real al00, al45, ni00, ni45, ag00, ag45
       integer mirror
c
       if ( metal .eq. 'A' ) then
               refl = al45(x)*al00(x)**(mirror-1)
       else if (metal .eq. 'N' ) then
               refl = ni45(x)*ni00(x)**(mirror-1)
       else if (metal .eq. 'S' ) then
               refl = ag45(x)*ag00(x)**(mirror-1)
       else if ( metal .eq. 'T' .or. mirror .eq. 0 ) then
               refl = 0.000575*x + 91.792
               refl = refl/100.0
       end if
c
       return
       end
c
c source spectrum finder
c
c data from Silva & Cornell 1992 ApJS 81, 865
c via http://zebu.uoregon.edu/spectra.html
       character*21 function filer(T,lines,label)
       real T
       character lines*1, label*30
       integer strend
c
       write (label, '(A,I6,A)') '\fiT\fn =', int(T), ' K'
       if ( lines .eq. 'l' .or. lines .eq. 'L' ) then
       if ( T .ge. 35000.0 ) then
               filer = 'library/O5V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (O5V)'
       else if ( T .ge. 27000.0 .and. T .lt. 35000.0 ) then
               filer = 'library/O7--B0V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (O7-B0V)'
       else if ( T .ge. 10700.00 .and. T .lt. 27000.0 ) then
               filer = 'library/B5III.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (B5III)'
       else if ( T .ge. 9200.0 .and. T .lt. 10700.0 ) then
               filer = 'library/A1--3V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (A1-3V)'
       else if ( T .ge. 8600.0 .and. T .lt. 9200.0 ) then
               filer = 'library/A5--7V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (A5-7V)'
       else if ( T .ge. 7800.0 .and. T .lt. 8600.0 ) then
               filer = 'library/A8V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (A8V)'
       else if ( T .ge. 6900.0 .and. T .lt. 7800.0 ) then
               filer = 'library/A9--F0V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (A9-F0V)'
       else if ( T .ge. 6500.0 .and. T .lt. 6900.0 ) then
               filer = 'library/F4-7III.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (F4-7III)'
       else if ( T .ge. 6300.0 .and. T .lt. 6500.0 ) then
               filer = 'library/F6--7V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (F6-7V)'
       else if ( T .ge. 6000.0 .and. T .lt. 6300.0 ) then
               filer = 'library/F8--9V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (F8-9V)'
       else if ( T .ge. 5700.0 .and. T .lt. 6000.0 ) then
               filer = 'library/G1--2V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (G1-2V)'
       else if ( T .ge. 5400.0 .and. T .lt. 5700.0 ) then
               filer = 'library/G5--6III.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (G5-6III)'
       else if ( T .ge. 5200.0 .and. T .lt. 5400.0 ) then
               filer = 'library/G9--K0V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (G9-K0V)'
       else if ( T .ge. 4900.0 .and. T .lt. 5200.0 ) then
               filer = 'library/K2III.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (K2III)'
       else if ( T .ge. 4600.0 .and. T .lt. 4900.0 ) then
               filer = 'library/K4V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (K4V)'
       else if ( T .ge. 4400.0 .and. T .lt. 4600.0 ) then
               filer = 'library/K5V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (K5V)'
       else if ( T .ge. 3600.0 .and. T .lt. 4400.0 ) then
               filer = 'library/K7III.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (K7III)'
       else if ( T .ge. 3300.0 .and. T .lt. 3600.0 ) then
               filer = 'library/M2V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (M2V)'
       else if ( T .lt. 3300.0 ) then
               filer = 'library/M4V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (M4V)'
       else
               filer = 'library/G1--2V.dat'
               write (label, '(A,I6,A,A)') '\fiT\fn =', int(T), ' K',
     $         ' (no match)'
       end if
       end if
c
       return
       end
c
c source light selector
       real function src(x,T,lines,spcwl,spcamp)
       real x, T, spcwl(1084), spcamp(1084)
       character*1 lines
c
       real bt
       integer nn, iii
       parameter ( nn = 1084 )
       logical keepgoing
c
       if ( lines .eq. 'k' .or. lines .eq. 'K' ) then
               src = bt(x,T)
       else if ( lines .eq. 'l' .or. lines .eq. 'L' ) then
               iii = 1
               keepgoing = .true.
11             if ( keepgoing .and. iii .le. 1083 ) then
                if ( x .ge. spcwl(iii) .and. x .le. spcwl(iii+1) ) then
                        if ( spcamp(iii) .ne. 0.0 .and.
     $                       spcamp(iii+1) .ne. 0.0 ) then
                        src = (spcamp(iii+1)-spcamp(iii))/
     $                        (spcwl(iii+1)-spcwl(iii))*
     $                        (x-spcwl(iii)) + spcamp(iii)
                        keepgoing = .false.
                        else
                        src = spcamp(iii)
                        keepgoing = .false.
                        end if
                end if
                iii = iii + 1
                goto 11
               end if
               if ( keepgoing ) then
                       src = 0.0
               end if
       end if
c
       return
       end
c
c
c
       subroutine valloc(dimen,array,mxvl,mxlc)
       integer dimen, mxlc, ii, jj
       real array(*), mxvl
c
       mxvl = 0.0
       do ii = 1, dimen
        do jj = 1, dimen
         if ( array(jj) .gt. array(ii) .and. array(jj) .gt. mxvl ) then
                 mxvl = array(jj)
                 mxlc = jj
         end if
        end do
       end do
c
       return
       end
c
c EOF
