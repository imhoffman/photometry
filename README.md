# Stellar Photometry Explorer

## [`phot.f`](phot.f)
A 900-line, 14-function, one-subroutine fortran program that uses an interactive terminal and [pgplot](http://www.astro.caltech.edu/~tjp/pgplot) window in an X Window environment to allow the user to plot UBVRI photometric measurements with respect to
* blackbody models or library spectra&mdash;Silva, David R. & Cornell, Mark E. "A new library of stellar optical spectra." Astrophysical Journal Supplement Series (ISSN 0067-0049), vol. 81, no. 2, Aug. 1992, p. 865&ndash;881 [doi:10.1086/191706](http://dx.doi.org/10.1086/191706)
* quantum efficiency curves for the [Apogee Instruments](http://www.andor.com/scientific-cameras/apogee-camera-range) AP8 (SITe-SIA003AB chip) and AP6 (KAF-1000 chip) [cameras](http://www.astrosurf.com/re/chip.html)
* reflection from two or three nickel, silver, or aluminum mirrors or transmission through one CaF<sub>2</sub> lens
* filter basspands&mdash;Bessell, M. S. "UBVRI passbands." Publications of the Astronomical Society of the Pacific, (1990) vol. 102, pp. 1181&ndash;1199 [doi:10.1086/132749](https://doi.org/10.1086/132749)

The program effectively deconvolves the telescope and optics from the data and writes out the corrected photometric values to the file [`phot.out`](phot.out) and prints the present state of the interactive window to a postscript file [`star.ps`](star.ps), thus presenting the user's match of the data to similarly corrected library spectra or simple blackbodies. If desired, the program can simply output the files without interaction if passed the single command-line argument ``p``.

```bash
$ gfortran -lm -ltk8.6 phot.f -lpgplot -o phot
$ ./phot p
```

When run interactively, the terminal offers the following menu.

```
Type "q" in the window to quit.
Type "6" or "8" to choose between AP6 and AP8 cameras.
Type "2" or "3" to choose number of mirror 'surfaces or "0" for a refractor.
Type "a" or "n" or "s" for mirror coatings:  Al, Ni, or Ag.
Type "v", "b", "r", or "i" to scale model to that data point.
Type "u" for default autoscaling.
Type "t" to enter temperature manually.
Type "k" for a blackbody source or "l" for a best match library spectrum.
Type "p" to write plot to star.ps and parameters to phot.out, then quit.
Type "h" for these instructions.
```

The bottom panel of the interactive display (and resulting star.ps file) shows all of the factors in the corrections.

![screen grab](grab.png)

* the magenta curve is the *r*<sup>2</sup> reflection coefficient (or *t*<sup>2</sup> transmission) for the chosen materials and geometry
* the gray curve is the quantum efficiency of the chosen chip
* the orange curve is the input spectrum, either a computed blackbody or a library file
* the blue, green, red, and brown curves are the BVRI filter passbands
* the filled blue, green, red, and brown areas are the functions that get integrated to yield the modelled value in the top panel

The top panel shows the data with vertical error bars for the measurement uncertainty quoted in the input file and horizontal bars showing the FWHM of the filter.

### example data and camera efficiencies
The repository includes several ``.phot`` files of example data. These data were taken at the Hawley Observatory at St. Paul's School using either the [70-cm Alumni Telescope](http://adsabs.harvard.edu/abs/2010AAS...21641402H) or a Takahashi refractor. The quantum efficiencies for the two camera chips used for data collection are provided.

### filter curves
The example data were taken with filters from [Custom Scientific](http://www.customscientific.com/index.html). Machine-readable data on these filters was obtained from the manufacturer&mdash;only plots are available on the website. We also obtained curves from [here](http://monet.uni-goettingen.de/foswiki/MonetObserving/Filters) for comparable filters from Omega. NB: the Omega curves do not entirely agree with the plots from Custom. Some postscript plots are available in the repository.

The filter passbands have an entry every 1.0&nbsp;nm. This is sufficiently fine resolution for the filter data vectors to be used for the wavelength domain coordinates in all calculations. All other wavelength-dependent properties are used as functions for which an interpolated value is returned. For example, during the numerical integration

```fortran
       do j = 1, k
        integ(j) = 0.0
        tot(j,1) = 0.0
        do i = 2, n
         integ(j) = integ(j) +
     $   ( bess(j,2,i-1)*qe(bess(j,1,i-1),camera)*
     $     src(bess(j,1,i-1),T,lines,spcwl,spcamp)*
     $     nphot(bess(j,1,i-1))*refl(bess(j,1,i-1),metal,mirror) +
     $     bess(j,2,i)*qe(bess(j,1,i),camera)*
     $     src(bess(j,1,i),T,lines,spcwl,spcamp)*
     $     nphot(bess(j,1,i))*refl(bess(j,1,i),metal,mirror)
     $   ) / 2.0 * ( bess(j,1,i) - bess(j,1,i-1) )
         tot(j,i)=bess(j,2,i)*qe(bess(j,1,i),camera)*
     $     src(bess(j,1,i),T,lines,spcwl,spcamp)*
     $     nphot(bess(j,1,i))*refl(bess(j,1,i),metal,mirror)
        end do
       end do
```

there are function calls for ``qe``, ``nphot``, ``refl``, and ``src`` at the wavelengths for which there is filter data (``bess``).

In general, the functions are implementations of offline fits to the data using ``gnuplot`` for which the fit coefficients are coded into the fortran and for which the details of the fits are recorded in [`fit.log`](materials/fit.log). One exception is the quantum efficiency for the AP6 camera which is read into ``phot`` and spline-interpolated at runtime.

### mirror reflections
In each case, a single 45-degree reflection is assumed and either one (Newtonian) or two (Cassegrain) normal reflections in the optical path. The reflectivities of nickel, silver, and aluminum at 45 degrees and normal were obtained from [luxpop](http://www.luxpop.com/).

The program can also model the calcium fluoride refractor used to take some of the data, in which case transmission through a single lens is included in the optical path.

## [`phot.dat`](phot.dat)
The input file of photometric data obtained at the telescope. These data would have been reduced offline in order to provide bias- and dark-corrected aperture counts from the chip. The format of this file is explained in comments within each of the supplied data files as

```
# entry file for program
# line 1: string for pgplot to display as main panel label
# line 2: temperature in Kelvin for star
# line 3: 'L' or 'K' for the initial display of a library or BB model
# line 4: 'N' or 'A' or 'S' for nickel, aluminum, or silver mirrors
# line 5: either '6' or '8' for ap6 or ap8 apogee camera
# line 6: integer number of reflections in telescope
# line 7: V band star counts and error in counts and exposure time
# line 8: B band
# line 9: R band
# line10: I band
# format (A1,E10.3,E10.3,F5.1), 'D' is data, else comment
```

Lines 4&ndash;6 allow for a description of the telescope and detector, lines 7&ndash;10 are the filtered data, and lines 1&ndash;3 are display options. In practice, the data files on disk are passed to the program through symbolic linking; for example

```bash
$ ln -s data/sigmaLeo.phot phot.dat
```

## [`oregon.f`](spectra/oregon.f)
The library spectra were obtained from [U Oregon](http://zebu.uoregon.edu/spectrar.html). This program reads the files and reformats them as simple two-column vectors for use by `phot`. It is used only for initial data formatting and is not needed at runtime.

